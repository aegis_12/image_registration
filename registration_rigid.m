
%   Coregistro rigido de imágenes

%   Imagen 1. ./data/brain.jpg
%   Imagen 2. ./data/brain2.jpg
%   Valores optimos. (tx,ty,theta,s) => (30,10,-10.5,1)

A = imread('./data/brain.jpg');     %   Referencia

B = imread('./data/brain2.jpg');    %   Movil

%   Parametros iniciales

s = 1;
theta = 1;
tx = 0;
ty = 0;

x0 = [tx, ty, theta];

%   Funcion coste

fun = @(x)Fcoste(A, transform(B, x(1), x(2), x(3), 1));

%B1 = transform(A, x(1), x(2), x(3), x(4));

%Fcoste(B, B1)

options = optimset('Display','iter','PlotFcns',@optimplotfval);
x = fminsearch(fun, x0, options);

C = transform(B, x(1), x(2), x(3), 1);

subplot(1,3,1), imshow(A);
subplot(1,3,2), imshow(B);
subplot(1,3,3), imshow(C);
