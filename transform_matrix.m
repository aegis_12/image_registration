function [ matrix ] = transform( tx,ty,theta,s )
    %   Matriz de transformacion
    
    matrix = [s*cosd(theta), s*sind(theta), tx;
              -s*sind(theta), s*cosd(theta), ty;
              0,             0,             1];
end