function [ coste ] = Fcoste(Ref, Image)
    %   Funcion de coste
    coste = abs(Ref - Image).^2;
    coste = coste(:);
    taman = numel(coste);
    coste = sum(coste) / taman;
end
