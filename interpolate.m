function [ P ] = interpolate( image, position )
    
    %   Funcion para interpolar pixeles
    
    x = position(1);
    y = position(2);
    
    x1 = floor(x);
    x2 = ceil(x);
    
    y1 = floor(y);
    y2 = ceil(y);
    
    %    Valor...
    
    base = (x2-x1) * (y2-y1);
    
    Q11 = ((x2-x) * (y2-y)) / (base) * image(x1,y1);
    Q21 = ((x-x1) * (y2-y)) / (base) * image(x2,y1);
    Q12 = ((x2-x) * (y-y1)) / (base) * image(x1,y2);
    Q22 = ((x-x1) * (y-y1)) / (base) * image(x2,y2);
    
    %    P...
    
    P = Q11 + Q21 + Q12 + Q22;
    
end