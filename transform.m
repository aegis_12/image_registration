function [ B ] = transform(Image, tx, ty, theta, s )
    %   Transformar imagen con una serie de cambios
    
    [sizeX, sizeY] = size(Image);
    B = zeros(size(Image), 'uint8');
    
    transfo_mat = transform_matrix(tx, ty, theta, s);
    
    for i=1:1:sizeX
       for j=1:1:sizeY
           tam = transfo_mat*[i;j;1];
           
           %    Limites
           if tam(1) < 1 || tam(1) > sizeX || tam(2) < 1 || tam(2) > sizeY
                continue
           end

           P = interpolate(Image, tam);
           B(i, j) = P;
           
       end
    end
end

